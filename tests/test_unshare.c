/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#include "munit.h"
#include "../src/isolate.h"

int main(int argc, char **argv)
{
	munit_assert_false(isolate_init());
}
