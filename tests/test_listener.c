/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#include "munit.h"

#include "../src/common.h"
#include "../src/listener.h"

#define BACKLOG 80 

int main(int argc, char **argv)
{
	// 1 - Caller listener(port)
	// 2 - Fork and attempt connection 
	// 3 - return response from connection attempt
	return -1;
}
