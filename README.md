# Convey 

IRCd with zero trust as a first citizen.

Convey is an attempt to create an IRC server and client, which embodies some of the zero-trust design ideals. That is to say, all flows shall be authorised, trusted devices, 2Factor authentication, and integration into identity services. While this project may not become a realisation, it hopes to provoke a discussion on the future or network services, their usability and security. Also! Did I mention that all these security mechanisms will be completely invisible to the end user, with minimal complexity to the service administrator. 

Policies will need to be designed and implemented, source code needs to be written, then audited, if this sounds interesting **contact me on matrix**. You should be able to find out how.  

## Goals of the project:

- Signed, reproducible builds
- Isolated using sandboxing techniques 
- Active monitoring: using prometheus 
- 100% code coverage
- Large set of unit tests
- Complete fuzz testing 
- Minimal dependencies. 
- Speak IRC
- Basic E2E chatting

## Features of the server:

- SAML 
- U2F, fall-back to TOTP.
- mTLS (maybe)
- microperimeterization - using traditional packet filtering
- Trusting clients' devices are secured (fully patched, no vulnerable services running); Drop down permissions if they fail.
- IPv6 only

## Feature of the client: 

- Interface with TPM  

# Design

High-Level Architecture

![Arch](docs/design/arch.png)

Software State Diagram

![Software State Diagram](docs/design/states.png)

## Users, Messages, and Channels

The figure below is a high-level diagram showing the states of the user (connect, disconnect), messages (send, receive), and the channels (join, leave). 

![Chat State Diagram](docs/design/chat-states.dot.png)


# See also

Below is collection of selected works that articulate, or exemplify certain aspects of the project. Be warned, some of these get fairly technical. 

- [Writing an IRCd](https://blog.summercat.com/writing-an-ircd.html)
- [Zero trust networks : building secure systems in untrusted networks by Barth, Doug and Gilman, Evan](http://gen.lib.rus.ec/search.php?req=Zero+Trust+Networks)
- [Set-by-step guide to mTLS](https://venilnoronha.io/a-step-by-step-guide-to-mtls-in-go)
- [A comparison of Unix sandboxing (2017)](https://www.engr.mun.ca/~anderson/publications/2017/sandbox-comparison.pdf)
- [CloudABI](https://www.jvt.me/posts/2017/02/14/cloudabi/) - See Google's Sandbox2
- [Linux namespaces](http://www.man7.org/conf/meetup/Linux-namespaces--jambit-Kerrisk-2019-05-20.pdf)
- [ZeroMQ](https://en.wikipedia.org/wiki/ZeroMQ)
- [Chromium Sandboxing](https://chromium.googlesource.com/chromium/src/+/master/docs/design/sandbox.md)
- [One Process in Many Namespaces](http://engineering.pivotal.io/post/goroutines-and-namespaces-part-1/#one-process-in-many-namespaces)
- [Google's Sandbox2](https://developers.google.com/sandboxed-api/docs/overview) - See CloudABI
- [CGroups](https://en.bmstu.wiki/CGroups_(Control_Groups))
- [Beej's Networking](https://beej.us/guide/bgnet)
- [API Security](https://wso2.com/whitepapers/wso2-whitepaper-building-an-ecosystem-for-api-security/)
	- [eXtensible Access Control Markup Language (XACML) Version 3.0](https://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-en.html)
- [Process Isolation in Firefox (and chrome via Mojo)](https://mozilla.github.io/firefox-browser-architecture/text/0012-process-isolation-in-firefox.html)
	- [Mozilla IPC Guide](https://wiki.mozilla.org/Security/Sandbox/IPCguide)
- [A Practical Guide to TPM 2.0](https://link.springer.com/book/10.1007%2F978-1-4302-6584-9)
	- [tpm2-software](https://github.com/tpm2-software)
- [Linux Containers in A Few Lines of Code](https://zserge.com/posts/containers/)
- [saltpack, a modern crypto messaging format](https://saltpack.org/) 
- [CBOR, RFC 7049 - Concise Binary Object Representation](http://cbor.io/)
- [Gocker - Docker in Golang](https://unixism.net/2020/06/containers-the-hard-way-gocker-a-mini-docker-written-in-go/)
- [WasmBoxC](https://kripken.github.io/blog/wasm/2020/07/27/wasmboxc.html)
- [Doors](https://en.wikipedia.org/wiki/Doors_(computing)) Sun Microsystems' procedure call to allow passing objects between processes, and even implement access control.
- [QUIC](https://en.wikipedia.org/wiki/QUIC) Userland replacement for TCP+TLS. Now [RFC9000](https://www.rfc-editor.org/rfc/rfc9000.html)
- [io_uring](https://unixism.net/loti/what_is_io_uring.html) a new asynchronous I/O API for Linux. 

# Source Code Instructions

Requirements

- meson
- ninja
- gcc/clang
- libc
- czmq 

Commands to install on Fedora: 

	dnf install meson ninja clang czmq-devel
	
Linux Requirements 

- linux namespaces

FreeBSD Requirements 

- capsicum

## Testing 

Requires [gcovr](https://gcovr.com/en/stable/) for the coverage tests, and [clang-analyzer](https://clang-analyzer.llvm.org/scan-build.html) for static analysis. Optionally, [lcov](http://ltp.sourceforge.net/coverage/lcov.php) can be used to generate HTML pages from the coverage information.

	dnf install gcovr lcov clang-analyzer

Run the tests, code coverage, and static analyser:

	meson build -Db_coverage=true 
	ninja -C build test
	ninja -C build coverage
	ninja -C build scan-build

## Compile Binary

	meson build
	ninja -C build
	./build/conveyd 'n;ps -aux'

# BSD-3-Clause

Copyright 2020 Peter Maynard, Ph.D. 

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
