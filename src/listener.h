/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#ifndef CV_LISTEN_H
#define CV_LISTEN_H

int listener(const char *port); 

#endif
