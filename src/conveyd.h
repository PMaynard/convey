/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#ifndef CV_DEAMON_H
#define CV_DEAMON_H

static void example1(void);

#endif
