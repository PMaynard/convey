/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#ifndef CV_ISOLATE_H
#define CV_ISOLATE_H

int isolate_init(void);

#endif
