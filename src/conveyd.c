/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting service for IRC communication.
License:     BSD-3-Clause
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "common.h"
#include "isolate.h"
#include "ipc.h"
#include "listener.h"
#include "conveyd.h"

static void example1(void) {
	int c;
	 FILE *fptr;

	 if ((fptr = fopen("/proc/net/dev","r")) == NULL)
			err(1, "fopen");

    while ((c = getc(fptr)) != EOF){
        putchar(c);

    }
	fclose(fptr); 
}

int main(int argc, char **argv)
{
	struct ipc i;
	i = ipc_init_rw(SHARED_MEM_NAME);

	char mybuf [256];

    while (1) {  // forever
        // Is there a string to print? P (spool_signal_sem);
        if (sem_wait (i.spool_signal_sem) == -1)
            err(1, "sem_wait: spool_signal_sem");
    
        strcpy (mybuf, i.shared_mem_ptr -> buf [i.shared_mem_ptr -> buffer_print_index]);

        /* Since there is only one process (the logger) using the 
           buffer_print_index, mutex semaphore is not necessary */
        (i.shared_mem_ptr -> buffer_print_index)++;
        if (i.shared_mem_ptr -> buffer_print_index == MAX_BUFFERS)
           i.shared_mem_ptr -> buffer_print_index = 0;

        /* Contents of one buffer has been printed.
           One more buffer is available for use by producers.
           Release buffer: V (buffer_count_sem);  */
        if (sem_post (i.buffer_count_sem) == -1)
            err(1, "sem_post: buffer_count_sem");
        
        printf("%s\n", mybuf);
    }

// Fork Management
	// return fork_management();


	// printf("Running example 1. Network accces\n");

	// example1();

	// isolate_init();

	// example1();

	// printf("Running Example 2. Command injection\n(Only with sandbox enabled)");

	// if (argc < 2)
	// 	err(1, "usage: %s 'n;ps -aux'\n", argv[0]);

// Example 2

	// char cmd[50], args[60];
	// strcat(cmd, "uname -");
	// strcat(args, argv[1]); // Example command injection.

	// strcat(cmd, args); 

	// if (system(cmd) == -1) 
	// 	err(1, "system");

// Listener
	// listener(NULL);

	exit(EXIT_SUCCESS);
}
