/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#ifndef CV_COMMON_H
#define CV_COMMON_H

#define PORT "6379" // Default Port 6379 (Hex for the first and last letter of the word 'convey')

int fork_management(void); 
struct addrinfo* getFirstIPv6(struct addrinfo *res);

#endif
