/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#include <sys/types.h>
#include <sys/socket.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>

#include "common.h"
#include "listener.h"

#define BACKLOG 20 

// TODO: Allow setting IP address to bind. e.g. "::1:6379"
int listener(const char *port){
	if (port == NULL)
		port = PORT;

	int error, s, new_s;
	// char ipv6_addstr[INET6_ADDRSTRLEN];
	int yes = 1;
	struct addrinfo hints, *res;
	struct sockaddr_storage client_addr;
	socklen_t addr_size;

	memset(&hints, 0, sizeof hints); // make sure the struct is empty
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	hints.ai_flags = AI_PASSIVE;     // Auto set IP. TODO: Allow setting IP address to bind.

	// perform network address and service translation
	if ((error = getaddrinfo(NULL, port, &hints, &res)))
		errx(1, "%s", gai_strerror(error));

	res = getFirstIPv6(res); 
	if (res == NULL)
		errx(1, "Unable to find an IPv6 Interface");

	if ((s = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1)
		err(1, "socket");

	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) 
		errx(1, "setsockopt");

	if ((error = bind(s, res->ai_addr, res->ai_addrlen) == -1))
		err(1, "bind");
	
	if (listen(s, BACKLOG))
		errx(1, "listen");

    freeaddrinfo(res); // free the linked list
	
	while(1) {
		addr_size = sizeof client_addr;
		if((new_s = accept(s, (struct sockaddr *)&client_addr, &addr_size)) == -1){
			errx(1, "accept");
			continue;
		}
        printf("server: got a connection \n");
        
        if (!fork()) { // this is the child process
            close(s); // child doesn't need the listener
            if (send(new_s, "Hello, world!", 13, 0) == -1)
                errx(1, "send");
            close(new_s);
            exit(0);
        }
        close(new_s);  // parent doesn't need this
	}

	return 0;
}
