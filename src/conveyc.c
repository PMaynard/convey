/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>

#include "common.h"

#define MAXDATASIZE 100 // max number of bytes we can get at once 

int main(int argc, char *argv[])
{
	char buf[MAXDATASIZE];
	char *port = PORT;
	int error, s, numbytes;
	struct addrinfo hints, *res;

	if (argc != 2 && argc != 3)
	    errx(1, "usage: client hostname [port]");

	if (argc == 3)
		port = argv[2];

	memset(&hints, 0, sizeof hints); // make sure the struct is empty
	hints.ai_family = AF_INET6;
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets

	// perform network address and service translation
	if ((error = getaddrinfo(argv[1], port, &hints, &res)))
		errx(1, "%s", gai_strerror(error));

	res = getFirstIPv6(res); 
	if (res == NULL)
		errx(1, "Unable to find an IPv6 Interface");

	if ((s = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1)
		err(1, "socket");

	if(connect(s, res->ai_addr, res->ai_addrlen) == -1)
		err(1, "connect"); 

    freeaddrinfo(res); // free the linked list

    if ((numbytes = recv(s, buf, MAXDATASIZE-1, 0)) == -1)
        errx(1, "recv");

    buf[numbytes] = '\0';
    printf("client: received '%s'\n",buf);
    close(s);

	return EXIT_SUCCESS;
}
