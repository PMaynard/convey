/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <unistd.h>
#include <err.h>

#include "ipc.h"

struct ipc ipc_open(const char *name, int flags) 
{
	struct ipc i;

	//  mutual exclusion semaphore, mutex_sem with an initial value 0.
	if ((i.mutex_sem = sem_open (SEM_MUTEX_NAME, O_CREAT, 0660, 0)) == SEM_FAILED)
		err(1, "sem_open");

	// Get shared memory 
	if ((i.fd_shm = shm_open(name, flags, 0660)) == -1)
		err(1,"shm_open");

	if (ftruncate (i.fd_shm, sizeof (struct shared_memory)) == -1)
		err(1, "ftruncate");

	if ((i.shared_mem_ptr = mmap (NULL, sizeof (struct shared_memory), PROT_READ | PROT_WRITE, MAP_SHARED, i.fd_shm, 0)) == MAP_FAILED)
	   err(1, "mmap");

	// Initialize the shared memory
	i.shared_mem_ptr -> buffer_index = i.shared_mem_ptr -> buffer_print_index = 0;

	// counting semaphore, indicating the number of available buffers. Initial value = MAX_BUFFERS
	if ((i.buffer_count_sem = sem_open (SEM_BUFFER_COUNT_NAME, O_CREAT, 0660, MAX_BUFFERS)) == SEM_FAILED)
		err(1, "sem_open");

	// counting semaphore, indicating the number of strings to be printed. Initial value = 0
	if ((i.spool_signal_sem = sem_open (SEM_SPOOL_SIGNAL_NAME, O_CREAT, 0660, 0)) == SEM_FAILED)
		err(1, "sem_open");

	// Initialization complete; now we can set mutex semaphore as 1 to 
	// indicate shared memory segment is available
	if (sem_post (i.mutex_sem) == -1)
		err(1, "sem_post: mutex_sem");

	return i;
}

// void ipc_init_r(const char *name, shared_memory *shared_mem_ptr, sem_t *spool_signal_sem)
// {
// 	return ipc_open(name, O_RDONLY | O_CREAT);
// }

struct ipc ipc_init_rw(const char *name) 
{
	return ipc_open(name, (O_RDWR | O_CREAT));
}