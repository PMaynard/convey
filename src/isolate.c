/*
Created By:  Peter Maynard <pete@port22.co.uk>
Created On:  2020 March 1st
Description: A sandboxed and zero-trusting client for IRC communication.
License:     BSD-3-Clause
*/

#define _GNU_SOURCE
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>

#include "common.h"

int isolate_init(void) {
	int res = -1; 
	res = unshare( CLONE_NEWUSER | CLONE_NEWUTS | CLONE_NEWPID | CLONE_NEWNET | CLONE_NEWNS | CLONE_NEWIPC);
	if ( res == -1)
		errx(1, "unshare");

	printf("\n\n[sandbox enabled.]\n\n");

	return res;
}
